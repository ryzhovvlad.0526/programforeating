﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace programforeating
{
    public class MainWindowVM
    {
        public ObservableCollection<string> Genders { get; set; }
        public MainWindowVM()
        {
            Genders = new ObservableCollection<string> { "М", "Ж" };
            SaveCmnd = new RelayCommand( x => DoSaveCmnd ); 
            

        }
        public string SelectedItem { get; set; }
        public int Age { get; set; }
        public ICommand SaveCmnd { get; set; }
        private void DoSaveCmnd() 
        { 
            
        }

    }
}
